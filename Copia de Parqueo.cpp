#include <iostream>
#include <vector>
#include <math.h>
#include <string>
#include <stdio.h>
#include <ctime>
#include <iomanip>

using namespace std;
time_t now;
struct tm nowLocal;
struct Reloj
{
	int hora;
	int minutos;
};
class Moneda
{
public:
	int denominacion;
	int cantidad_monedas;
	int cantidad_carga;
	int cantidad_entradas;
	int cantidad_salidas;
	Moneda(int den)
	{
		denominacion=den;
		cantidad_monedas=0;
		cantidad_carga=0;
		cantidad_entradas=0;
		cantidad_salidas=0;
	}
};
class Billete
{
public:
	int denominacion;
	int cantidad_billetes;
	int cantidad_carga;
	int cantidad_entradas;
	int cantidad_salidas;
	Billete(int den)
	{
		denominacion=den;
		cantidad_billetes=0;
		cantidad_carga=0;
		cantidad_entradas=0;
		cantidad_salidas=0;
	}
};
struct Fecha
{
	int dia;
	int mes;
	int year;
};
//Creacion clase carro.
class Parqueo
{
private:
	int numero_espacio;
	string placa;
	Fecha fecha_entrada;
	Reloj hora_entrada;
	Fecha fecha_pago;
	Reloj hora_pago;
	int valor_pagado;
	Fecha fecha_salida;
	Reloj hora_salida;
public:
	Parqueo(int);
	void asignar_placa(string);
	string obtener_placa();
	void asignar_fecha_hora_entrada();
	Reloj obtener_hora_entrada();
	Fecha obtener_fecha_entrada();
	void asignar_fecha_hora_salida();
	int obtener_campo();
	void imprime_hora_fecha_entrada();
	Reloj obtener_hora_pago();
	Fecha obtener_fecha_pago();
	void reset_campo();
	void asignar_fecha_hora_entrada_recargo();

};
Reloj Parqueo::obtener_hora_entrada()
{
	return hora_entrada;
}
Fecha Parqueo::obtener_fecha_entrada()
{
	return fecha_entrada;
}
void Parqueo::asignar_fecha_hora_entrada_recargo()
{
	fecha_entrada.dia=fecha_pago.dia;
	fecha_entrada.mes=fecha_pago.mes;
	fecha_entrada.year=fecha_pago.year;
	hora_entrada.hora=hora_pago.hora;
	hora_entrada.minutos=hora_pago.minutos;
}
Parqueo::Parqueo(int _numero_espacio)
{
	numero_espacio=_numero_espacio;
	placa="";
	fecha_entrada.dia=0;
	fecha_entrada.mes=0;
	fecha_entrada.year=0;
	hora_entrada.hora=0;
	hora_entrada.minutos=0;
	fecha_pago.dia=0;
	fecha_pago.mes=0;
	fecha_pago.year=0;
	hora_pago.hora=11;
	hora_pago.minutos=15;
	valor_pagado=0;
	fecha_salida.dia=0;
	fecha_salida.mes=0;
	fecha_salida.year=0;
	hora_salida.hora=0;
	hora_salida.minutos=0;
}
void Parqueo::asignar_placa(string _placa)
{
	placa=_placa;
}
string Parqueo::obtener_placa()
{
	return placa;
}
void Parqueo::asignar_fecha_hora_entrada()
{
	hora_entrada.hora=nowLocal.tm_hour;
	hora_entrada.minutos=nowLocal.tm_min;
	fecha_entrada.dia=nowLocal.tm_mday;
	fecha_entrada.mes=nowLocal.tm_mon+1;
	fecha_entrada.year=nowLocal.tm_year+1900;
}
int Parqueo::obtener_campo()
{
	return numero_espacio;
}
void Parqueo::imprime_hora_fecha_entrada()
{
	cout<<hora_entrada.hora<<":"<<hora_entrada.minutos<<"  ";
	cout<<fecha_entrada.dia<<"/"<<fecha_entrada.mes<<"/"<<fecha_entrada.year<<endl;
}
Reloj Parqueo::obtener_hora_pago()
{
	return hora_pago;
}
Fecha Parqueo::obtener_fecha_pago()
{
	return fecha_pago;
}
void Parqueo::asignar_fecha_hora_salida()
{
	hora_salida.hora=nowLocal.tm_hour;
	hora_salida.minutos=nowLocal.tm_min;
	fecha_salida.dia=nowLocal.tm_mday;
	fecha_salida.mes=nowLocal.tm_mon+1;
	fecha_salida.year=nowLocal.tm_year+1900;
}
void Parqueo::reset_campo()
{
	placa="";
	fecha_entrada.dia=0;
	fecha_entrada.mes=0;
	fecha_entrada.year=0;
	hora_entrada.hora=0;
	hora_entrada.minutos=0;
	fecha_pago.dia=0;
	fecha_pago.mes=0;
	fecha_pago.year=0;
	hora_pago.hora=0;
	hora_pago.minutos=0;
	valor_pagado=0;
	fecha_salida.dia=0;
	fecha_salida.mes=0;
	fecha_salida.year=0;
	hora_salida.hora=0;
	hora_salida.minutos=0;
}
bool existe_placa(string plak,vector<Parqueo>espacio_parqueo)
{
	for(int i=0;i<espacio_parqueo.size();i++)
	{
		if(espacio_parqueo[i].obtener_placa()==plak)
		{
			return true;
		}
	}
	return false;
}
bool todo_en_orden(int cantidad, vector<int>plata)
{
	for(int i=0;i<cantidad;i++)
	{
		if(plata[i]!=0)
		{
			if(plata[i]<0)
			{
				return false;
			}
		}
	}
	for(int i=0;i<cantidad-1;i++)
	{
		if(plata[i]!=0)
		{
			if(plata[i]>=plata[i+1])
			{
				return false;
			}
		}
	}
	return true;
}
int main()
{
	int opcion=100;
	int cantidad_espacios;
	int pago_minimo;
	int estado;
	int minutos_salir;
	int redondeo;
	int moneda;
	int billete;
	float precio_hora;
	int total_monedas;
	int total_billetes;
	vector<int>monedas;
	vector<Moneda>control_monedas;
	vector<int>billetes;
	vector<Billete>control_billetes;
	vector<Parqueo>espacio_parqueo;
	vector<Parqueo>detalle_de_uso;
	int cantidad_total_entrada;
	int plata_total_entrada;
	int cantidad_total_salidas;
	int plata_total_salidas;
	int cantidad_total_saldo;
	int plata_total_saldo;
	string plac;
	int espacios_disponibles;
	int espacio_asignado;

while(opcion!=0)
{
	cout<<"1. Configuración \n"<<"2. Cargar Cajero\n"<<"3. Saldo del cajero\n"<<"4. Ingresos de dinero\n"<<"5. Entrada de vehículo"<<endl;
	cout<<"6. Cajero del parqueo\n"<<"7. Salida del vehículo\n"<<"8. Ayuda\n"<<"9. Acerda de\n"<<"0.Fin"<<endl;
	cout<<"Indique la opción: "<<endl;
	cin>>opcion;

	switch(opcion)
	{
		case 1:
			control_monedas.clear();
			control_billetes.clear();
			cout<<right<<setw(40)<<"CONFIGURACION"<<endl<<endl;
			cout<<"Cantidad de espacios en el parqueo: ";
			cin>>cantidad_espacios;
			while(cantidad_espacios<1)
			{
				cout<<"Debe indicar un numero mayor o igual a 1"<<endl;
				cout<<"Cantidad de espacios en el parqueo: ";
				cin>>cantidad_espacios;
			}
			cout<<"Precio por hora: ";
			cin>>precio_hora;
			while(precio_hora<0)
			{
				cout<<"Debe indicar un precio mayor o igual a cero"<<endl;
				cout<<"Precio por hora: ";
				cin>>precio_hora;
			}
			cout<<"Pago mínimo: ";
			cin>>pago_minimo;
			while(pago_minimo<0)
			{
				cout<<"Debe indicar un precio mayor o igual a cero"<<endl;
				cout<<"Pago mínimo: ";
				cin>>pago_minimo;
			}
			cout<<"Redondear cobro a los siguientes minutos: ";
			cin>>redondeo;
			while(redondeo<=0 || redondeo>=60)
			{
				cout<<"Debe indicar un numero entre 0 y 60"<<endl;
				cout<<"Redondear el cobro a los siguientes minutos: ";
				cin>>redondeo;
			}
			cout<<"Minutos maximos para salir despues del pago: ";
			cin>>minutos_salir;
			while(minutos_salir<0)
			{
				cout<<"Debe indicar un numero mayor o igual a 0"<<endl;
				cout<<"Minutos maximos para salir despues del pago: ";
				cin>>minutos_salir;
			}
			cout<<"Tipos de moneda"<<endl;
			monedas.clear();
			total_monedas=0;
			for(int i=0;i<3;i++)
			{
				cout<<right<<setw(5)<<"Moneda "<<i+1<<": ";
				cin>>moneda;
				if(moneda==0)
				{
					while(i<3)
					{
						monedas.push_back(0);
						i++;
					}
					break;
				}
				monedas.push_back(moneda);
				total_monedas++;
			}
			while(todo_en_orden(total_monedas,monedas)==false)
			{
				cout<<"Las monedas deben ser mayores que cero y debe indicarlas de menor a mayor denominacion\n Vuelva a indicar las monedas\n";
				monedas.clear();
				total_monedas=0;
				for(int i=0;i<3;i++)
				{
					cout<<right<<setw(5)<<"Moneda "<<i+1<<": ";
					cin>>moneda;
					if(moneda==0)
					{
						while(i<3)
						{
							monedas.push_back(0);
							i++;
						}
						break;
					}
					monedas.push_back(moneda);
					total_monedas++;
				}
			}
			cout<<"Tipos de billetes"<<endl;
			billetes.clear();
			total_billetes=0;
			for(int i=0;i<5;i++)
			{
				cout<<right<<setw(5)<<"Billete "<<i+1<<": ";
				cin>>billete;
				if(billete==0)
				{
					while(i<5)
					{
						billetes.push_back(0);
						i++;
					}
					break;
				}
				billetes.push_back(billete);
				total_billetes++;
			}
			while(todo_en_orden(total_billetes,billetes)==false)
			{
				cout<<"Los billetes deben ser mayores que cero y debe indicarlas de menor a mayor denominacion\n Vuelva a indicar los billetes\n";
				billetes.clear();
				total_billetes=0;
				for(int i=0;i<5;i++)
				{
					cout<<right<<setw(5)<<"Billete "<<i+1<<": ";
					cin>>billete;
					if(billete==0)
					{
						while(i<5)
						{
							billetes.push_back(0);
							i++;
						}
						break;
					}
					billetes.push_back(billete);
					total_billetes++;
				}
			}
			for(int i=1;i<=cantidad_espacios;i++)
			{
				Parqueo p(i);
				espacio_parqueo.push_back(p);
			}
			for(int j=0;j<total_monedas;j++)
			{
				Moneda m(monedas[j]);
				control_monedas.push_back(m);
			}
			for(int j=0;j<total_billetes;j++)
			{
				Billete b(billetes[j]);
				control_billetes.push_back(b);
			}
			break;
		case 2:
		case 3:
			cantidad_total_entrada=0;
			plata_total_entrada=0;
			cantidad_total_salidas=0;
			plata_total_salidas=0;
			cantidad_total_saldo=0;
			plata_total_saldo=0;
			cout<<right<<setw(60)<<"SALDO DEL CAJERO"<<endl;
			cout<<right<<setw(35)<<"ENTRADAS";
			cout<<right<<setw(35)<<"SALIDAS";
			cout<<right<<setw(35)<<"SALDO"<<endl;
			cout<<left<<setw(20)<<"DENOMINACION"<<setw(20)<<"CANTIDAD"<<setw(15)<<"TOTAL"<<setw(20)<<"CANTIDAD"<<setw(20)<<"TOTAL"<<setw(20)<<"CANTIDAD"<<setw(15)<<"TOTAL"<<endl<<endl;
			for (int i=0;i<total_monedas;i++)
			{
				cout<<left<<setw(10)<<"Monedas de "<<setw(13)<<control_monedas[i].denominacion;
				cout<<left<<setw(20)<<control_monedas[i].cantidad_entradas;
				cout<<left<<setw(15)<<control_monedas[i].denominacion*control_monedas[i].cantidad_entradas;
				cout<<left<<setw(20)<<control_monedas[i].cantidad_salidas;
				cout<<left<<setw(20)<<control_monedas[i].cantidad_salidas*control_monedas[i].denominacion;
				cout<<left<<setw(20)<<control_monedas[i].cantidad_entradas-control_monedas[i].cantidad_salidas;
				cout<<(control_monedas[i].cantidad_entradas-control_monedas[i].cantidad_salidas)*control_monedas[i].denominacion<<endl;
				cantidad_total_entrada=cantidad_total_entrada+control_monedas[i].cantidad_entradas;
				plata_total_entrada=plata_total_entrada+(control_monedas[i].cantidad_entradas*control_monedas[i].denominacion);
				cantidad_total_salidas=cantidad_total_salidas+control_monedas[i].cantidad_salidas;
				plata_total_salidas=plata_total_salidas+(control_monedas[i].cantidad_salidas*control_monedas[i].denominacion);
				cantidad_total_saldo=cantidad_total_saldo+control_monedas[i].cantidad_entradas-control_monedas[i].cantidad_salidas;
				plata_total_saldo=plata_total_saldo+(control_monedas[i].cantidad_entradas-control_monedas[i].cantidad_salidas)*control_monedas[i].denominacion;
			}
			cout<<left<<setw(24)<<"TOTAL DE MONEDAS"<<setw(20)<<cantidad_total_entrada;
			cout<<left<<setw(15)<<plata_total_entrada;
			cout<<left<<setw(20)<<cantidad_total_salidas;
			cout<<left<<setw(20)<<plata_total_salidas;
			cout<<left<<setw(20)<<cantidad_total_saldo;
			cout<<plata_total_saldo<<endl<<endl;

			cantidad_total_entrada=0;
			plata_total_entrada=0;
			cantidad_total_salidas=0;
			plata_total_salidas=0;
			cantidad_total_saldo=0;
			plata_total_saldo=0;

			for (int i=0;i<total_billetes;i++)
			{
				cout<<left<<setw(7)<<"Billetes de "<<setw(12)<<control_billetes[i].denominacion;
				cout<<left<<setw(20)<<control_billetes[i].cantidad_entradas;
				cout<<left<<setw(15)<<control_billetes[i].denominacion*control_billetes[i].cantidad_entradas;
				cout<<left<<setw(20)<<control_billetes[i].cantidad_salidas;
				cout<<left<<setw(20)<<control_billetes[i].cantidad_salidas*control_billetes[i].denominacion;
				cout<<left<<setw(20)<<control_billetes[i].cantidad_entradas-control_billetes[i].cantidad_salidas;
				cout<<(control_billetes[i].cantidad_entradas-control_billetes[i].cantidad_salidas)*control_billetes[i].denominacion<<endl;
				cantidad_total_entrada=cantidad_total_entrada+control_billetes[i].cantidad_entradas;
				plata_total_entrada=plata_total_entrada+(control_billetes[i].cantidad_entradas*control_billetes[i].denominacion);
				cantidad_total_salidas=cantidad_total_salidas+control_billetes[i].cantidad_salidas;
				plata_total_salidas=plata_total_salidas+(control_billetes[i].cantidad_salidas*control_billetes[i].denominacion);
				cantidad_total_saldo=cantidad_total_saldo+control_billetes[i].cantidad_entradas-control_billetes[i].cantidad_salidas;
				plata_total_saldo=plata_total_saldo+(control_billetes[i].cantidad_entradas-control_billetes[i].cantidad_salidas)*control_billetes[i].denominacion;
			}
			cout<<left<<setw(24)<<"TOTAL DE BILLETES"<<setw(20)<<cantidad_total_entrada;
			cout<<left<<setw(15)<<plata_total_entrada;
			cout<<left<<setw(20)<<cantidad_total_salidas;
			cout<<left<<setw(20)<<plata_total_salidas;
			cout<<left<<setw(20)<<cantidad_total_saldo;
			cout<<plata_total_saldo<<endl<<endl;
			break;
			case 5:
				now=time(NULL);
				nowLocal=*localtime(&now);
				espacios_disponibles=0;
				cout<<"Indique su placa: "<<endl;
				cin.ignore();
				getline(cin,plac);
				if(existe_placa(plac,espacio_parqueo)==true)
				{
					cout<<"Ya esta la placa de este vehiculo registrada, no puede entrar"<<endl;
					break;
				}

				if(existe_placa(plac,espacio_parqueo)==false)
				{
					for(int i=0;i<espacio_parqueo.size();i++)
					{
						if(espacio_parqueo[i].obtener_placa()=="")
						{
							espacios_disponibles++;
						}
					}

					if(espacios_disponibles!=0)
					{
						for(int i=0;i<espacio_parqueo.size();i++)
						{
							if(espacio_parqueo[i].obtener_placa()=="")
							{
								espacio_parqueo[i].asignar_placa(plac);
								espacio_parqueo[i].asignar_fecha_hora_entrada();
								espacio_asignado=i;
								break;
							}
						}
						cout<<right<<setw(50)<<"ENTRADA DEL VEHICULO"<<endl;
						cout<<"Espacios disponibles"<<right<<setw(20)<<espacios_disponibles<<endl;
						cout<<"SU PLACA"<<right<<setw(20)<<espacio_parqueo[espacio_asignado].obtener_placa()<<endl;
						cout<<"Campo asignado"<<right<<setw(20)<<espacio_parqueo[espacio_asignado].obtener_campo()<<endl;
						cout<<"Hora entrada"<<right<<setw(15);
						espacio_parqueo[espacio_asignado].imprime_hora_fecha_entrada();
						cout<<"Precio por hora      "<<precio_hora<<endl;
						cout<<"Cobro minimo de tiempo"<<right<<setw(20)<<pago_minimo<<endl;
						cout<<"Vehiculo registrado"<<endl;
					}
					if(espacios_disponibles==-0)
					{
						cout<<"NO HAY ESPACIO"<<endl;
					}
				}
				break;
				case 6:
					cout<<"Indique su placa: ";
					cin.ignore();
					getline(cin,plac);
					for(int i=0;i<espacio_parqueo.size();i++)
					{
						if(espacio_parqueo[i].obtener_placa()==plac)
						{
							cout<<left<<setw(15)<<"SU PLACA"<<espacio_parqueo[i].obtener_placa();
						}
					}
					break;
				case 7:
					now=time(NULL);
					nowLocal=*localtime(&now);
				 	cout<<"Indique la placa del vehiculo: ";
					cin.ignore();
					getline(cin,plac);
					if(existe_placa(plac,espacio_parqueo)==false)
					{
						cout<<"La placa no esta registrada en el parqueo."<<endl;
						break;
					}
					for(int i=0;i<espacio_parqueo.size();i++)
					{
						if(espacio_parqueo[i].obtener_placa()==plac)
						{
							if((((nowLocal.tm_hour-espacio_parqueo[i].obtener_hora_pago().hora)*60)+nowLocal.tm_min-espacio_parqueo[i].obtener_hora_pago().minutos)>minutos_salir)
							{
								cout<<"No puede salir porque excedió el tiempo permitido para ello."<<endl;
								cout<<left<<setw(15)<<"Tiempo máximo para salir "<<minutos_salir<<" minutos"<<endl;
								cout<<left<<setw(15)<<"Tiempo que usted ha tardado "<<(nowLocal.tm_hour-espacio_parqueo[i].obtener_hora_pago().hora)*60+(nowLocal.tm_min-espacio_parqueo[i].obtener_hora_pago().minutos)<<" minutos"<<endl;
								cout<<"Debe regresar al cajero a pagar la diferencia"<<endl;
								espacio_parqueo[i].asignar_fecha_hora_entrada_recargo();
								break;
							}
							if((((nowLocal.tm_hour-espacio_parqueo[i].obtener_hora_pago().hora)*60)+nowLocal.tm_min-espacio_parqueo[i].obtener_hora_pago().minutos)<=minutos_salir)
							{
								cout<<"SALIDA DEL VECHICULO\n";
								cout<<left<<setw(15)<<"SU PLACA"<<espacio_parqueo[i].obtener_placa()<<endl;
								cout<<"Salida registrada."<<endl;
								espacio_parqueo[i].asignar_fecha_hora_salida();
								detalle_de_uso.push_back(espacio_parqueo[i]);
								espacio_parqueo[i].reset_campo();
							}
						}
					}













	}
}
}
